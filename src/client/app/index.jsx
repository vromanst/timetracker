import React from 'react';
import {render} from 'react-dom';

import TimeComponent from './components/TimeComponent.jsx';


class App extends React.Component {
  render () {
    return <TimeComponent />;
  }
}

render(<App/>, document.getElementById('app'));
