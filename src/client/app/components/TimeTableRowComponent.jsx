import React from 'react';

class TimeTableRowComponent extends React.Component {

  constructor(props) {    
    super(props);

    this.state = this._stateFromProps(props);
  }

  _stateFromProps(props){
    return {
      activity: props.activity
    };
  }

  render() {
    return (
        <tr>
          <td>{this.state.activity.activity}</td>
          <td>{this.state.activity.timestamp}</td>
        </tr>
    );
  }

}

export default TimeTableRowComponent;
