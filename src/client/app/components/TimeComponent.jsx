import React from 'react';
import update from 'react-addons-update';

import TimeTableRowComponent from './TimeTableRowComponent.jsx';

class TimeComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = this._stateFromProps(props);
    this._onAdd = this._onAdd.bind(this);
  }

  _stateFromProps(props) {
    return {
      activities: [],
      inputValue: ''
    };
  }

  _onAdd () {
    let d = new Date();
    let newActivity = this.state.activities.concat({id: this.state.activities.length+1, activity: this.state.inputValue, timestamp: d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()});
    let newState = update(this.state, { $merge: {activities: newActivity} });
    this.setState(newState);
  }

  _handleChange(event) {
    this.setState({inputValue: event.target.value});
  }

  render() {
    return (
      <div className="container" style={{marginLeft: 10+'px'}}>
        <div className="row">        
        <div className="col-md-6">
        <table className="table table-striped">
          <thead style={{backgroundColor:'#2e6da4', color:"#fff"}}>
            <tr>
            <th>Activity</th>
            <th>Time</th>
            </tr>
          </thead>
          <tbody>        
          {this.state.activities.map(function(a){
            return (<TimeTableRowComponent activity={a} key={a.id} />);
          })}
          </tbody>
        </table>
        </div>
        <div className="col-md-3">
          <div className="input-group" style={{marginTop:10+'px'}}>
          <input type="text" size="20" placeholder="Activity" className="form-control"  defaultValue={this.state.inputValue} onBlur={this._handleChange.bind(this)}></input>
          <span onClick={this._onAdd} className="btn btn-primary input-group-addon">Add</span>
          </div>
        </div>
      </div>
      </div>
    );
  }

}

export default TimeComponent;
